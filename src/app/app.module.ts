import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// Pages
import { ContactPage } from '../pages/contact/contact';
import { MapPage } from '../pages/map/map';
import { TabsPage } from '../pages/tabs/tabs';
import { WeatherDetailsPage } from '../pages/weather-details/weather-details';
// Libraries
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { HttpClientModule } from '@angular/common/http';
// Providers
import { WeatherProvider } from '../providers/weather/weather';
import { NetworkProvider } from '../providers/network/network';
import { DataPassingProvider } from '../providers/data-passing/data-passing';

@NgModule({
    declarations: [
        MyApp,
        ContactPage,
        MapPage,
        TabsPage,
        WeatherDetailsPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: 'WeatherDB',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        HttpClientModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        ContactPage,
        MapPage,
        TabsPage,
        WeatherDetailsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        GoogleMaps,
        Geolocation,
        Network,
        NativeGeocoder,
        WeatherProvider,
        NetworkProvider,
        DataPassingProvider
    ]
})
export class AppModule { }
