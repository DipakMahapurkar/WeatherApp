import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, ToastController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {
    GoogleMaps,
    GoogleMap,
    CameraPosition,
    LatLng,
    GoogleMapsEvent,
    GoogleMapOptions,
    Marker,
    MarkerOptions, GoogleMapsAnimation
} from '@ionic-native/google-maps';

import { Geolocation } from '@ionic-native/geolocation'
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { WeatherDetailsPage } from '../weather-details/weather-details'
import { WeatherProvider } from '../../providers/weather/weather';
import { NetworkProvider } from '../../providers/network/network';
import { DataPassingProvider } from '../../providers/data-passing/data-passing';

declare var google;

@Component({
    selector: 'page-map',
    templateUrl: 'map.html'
})
export class MapPage {
    // Element reference to html child in DOM
    @ViewChild('map_canvas') mapElement: ElementRef;
    @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

    // Class member declaration and initialization
    map: GoogleMap;
    loc: LatLng;
    weather: any;
    location = {};
    newPosition: LatLng;
    latitude: number;
    longitude: number;
    autocompleteService: any;
    placesService: any;
    query: string = '';
    places: any = [];
    cityName: string = '';

    constructor(public navCtrl: NavController,
        private _googleMaps: GoogleMaps,
        private _geoLoc: Geolocation,
        private toastCtrl: ToastController,
        private storage: Storage,
        public viewCtrl: ViewController,
        public zone: NgZone,
        private nativeGeocoder: NativeGeocoder,
        private weatherProvider: WeatherProvider,
        private networkProvider: NetworkProvider,
        private dataPassProvider: DataPassingProvider
    ) {

    }

    ngAfterViewInit() {
        this.initMap();
        //once the map is ready move camera into position
        this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
            // Get Current location of user.
            this.getCurrentLocationOfUser();
            // Initialize autocomplete service
            this.autocompleteService = new google.maps.places.AutocompleteService();
        });
    };

    //Load the map 
    initMap() {
        let element = this.mapElement.nativeElement;
        // Create a map after the view is loaded.
        // Google map option properties
        let mapOptions: GoogleMapOptions = {
            enableHighAccuracy: true,
            disableDefaultUI: true,
            camera: {
                zoom: 18,
                tilt: 30
            }
        };
        // Create element for map with above options.
        this.map = this._googleMaps.create(element, mapOptions)
    };

    // Get current user location
    getCurrentLocationOfUser() {
        //Get User location
        this.getLocation().then(res => {
            // Clear previous marker
            this.map.clear();
            //Once location is gotten, we set the location on the camera.
            this.loc = new LatLng(res.coords.latitude, res.coords.longitude);
            this.moveCamera(this.loc);

            // Get full address on lat lng by using Native Geolocation Plugin.
            this.getAddressFromLatLong(res.coords.latitude, res.coords.longitude)
                .then((result: NativeGeocoderReverseResult[]) => {
                    console.log(JSON.stringify(result[0]));
                    // Set city for weather details
                    this.cityName = result[0].subLocality;
                    //Create marker with info window for address show
                    this.createMarker(this.loc, '' + result[0].subAdministrativeArea + ', ' + result[0].subLocality + ', ' + result[0].locality + ', ' + result[0].administrativeArea + ', ' + result[0].countryName + ', ' + result[0].postalCode)
                        .then((marker: Marker) => {
                            // Show address in info window
                            marker.showInfoWindow();
                            // If clicked it, Go to weather detail page
                            marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                                // Check internet connection
                                if (this.networkProvider.isOnline()) {
                                    // Get weather details from lat, lng
                                    this.getWeatherDetailsFromLocation(res.coords.latitude, res.coords.longitude);
                                } else {
                                    this.showToast('Please check your internet connection !!');
                                    setTimeout(() => {
                                        this.navCtrl.push(WeatherDetailsPage);
                                    }, 10)
                                }
                            });
                        }).catch(err => {
                            this.showToast(err);
                        });
                }).catch((error: any) => {
                    this.showToast(error);
                });
        }).catch(err => {
            this.showToast(err);
        });
    };

    //Get current user location
    //Returns promise
    getLocation() {
        // Options for location
        let options = {
            timeout: 30000,
            enableHighAccuracy: true,
            maximumAge: 3600
        };
        return this._geoLoc.getCurrentPosition(options);
    };

    //Moves the camera to any location
    moveCamera(loc: LatLng) {
        let options: CameraPosition<LatLng> = {
            //specify center of map
            target: loc,
            zoom: 17,
            tilt: 15
        }
        // Animate camera
        this.map.animateCamera(options)
    };

    //Adds a marker to the map
    createMarker(loc: LatLng, title: string) {
        let markerOptions: MarkerOptions = {
            position: loc,
            title: title,
            animation: GoogleMapsAnimation.BOUNCE,
            draggable: true,
        };
        return this.map.addMarker(markerOptions);
    };

    // Get full address from lat, lng
    // Return promise 
    getAddressFromLatLong(lat, lng) {
        let options: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };
        return this.nativeGeocoder.reverseGeocode(lat, lng, options);
    };

    // Get weather details 
    getWeatherDetailsFromLocation(longitude, latitude) {
        this.weatherProvider.currentWeather(longitude, latitude).subscribe(res => {
            if (res.length > 0) {
                this.location = {
                    id: res[0].id,
                    icon: `http://openweathermap.org/img/w/${res[0].weather[0].icon}.png`,
                    current: res[0],
                    cityName: this.cityName
                }
                console.log('Location Details : ' + JSON.stringify(this.location));
                this.storage.set('WEATHER_DETAILS', JSON.stringify(this.location));
                this.dataPassProvider.setData(this.location);
                this.navCtrl.push(WeatherDetailsPage);
            }
        });
    };

    // On search of place select the place
    selectPlace(place) {
        // Too initialize Place service after map initialize and created dummy div to run place service.
        this.placesService = new google.maps.places.PlacesService(document.createElement('div'));
        this.places = []; // Too initialize places array for auto search places.
        let location = {
            lat: null,
            lng: null,
            name: place.name
        };

        // On Click of auto search list get location details.
        this.placesService.getDetails({ placeId: place.place_id }, (details) => {
            this.zone.run(() => {
                location.name = details.name;
                location.lat = details.geometry.location.lat();
                location.lng = details.geometry.location.lng();
                this.newPosition = new LatLng(location.lat, location.lng);
                this.moveCamera(this.newPosition);

                this.getAddressFromLatLong(location.lat, location.lng)
                    .then((result: NativeGeocoderReverseResult[]) => {
                        console.log(JSON.stringify(result[0]));
                        this.cityName = result[0].subLocality;
                        console.log("New Location Cords : " + JSON.stringify(this.newPosition));
                        // Marker create on selected place.
                        this.createMarker(this.newPosition, '' + result[0].subAdministrativeArea + ', ' + result[0].subLocality + ', ' + result[0].locality + ', ' + result[0].administrativeArea + ', ' + result[0].countryName + ', ' + result[0].postalCode)
                            .then((marker: Marker) => {
                                // showing address in info window.
                                marker.showInfoWindow();
                                // If clicked it, Go to weather detail page
                                marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                                    if (this.networkProvider.isOnline()) {
                                        // Get weather details from on selected lat lng for that place.
                                        this.getWeatherDetailsFromLocation(location.lat, location.lng);
                                    } else {
                                        this.showToast('Please check your internet connection !!');
                                        setTimeout(()=>{
                                            this.navCtrl.push(WeatherDetailsPage);
                                        });                                                                                
                                    }
                                });
                            }).catch(err => {
                                // Showing error message in toast
                                this.showToast(err);
                            });
                    }).catch((error: any) => {
                        console.log(error)
                        // Showing error message in toast
                        this.showToast(error);
                    });
            });

        });
    };

    // Search place and show list for matching places
    searchPlace() {
        if (this.query.length > 0) {
            let config = {
                types: ['geocode'],
                input: this.query
            }
            this.autocompleteService.getPlacePredictions(config, (predictions, status) => {
                if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {
                    this.places = [];
                    predictions.forEach((prediction) => {
                        this.places.push(prediction);
                    });
                }
            });
        } else {
            this.places = [];
        }
    };

    // Show Error message in toast
    showToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
}