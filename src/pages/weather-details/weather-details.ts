import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

// Providers
import { NetworkProvider } from '../../providers/network/network';
import { DataPassingProvider } from '../../providers/data-passing/data-passing';

@IonicPage()
@Component({
    selector: 'page-weather-details',
    templateUrl: 'weather-details.html',
})
export class WeatherDetailsPage {
    // Class members declaration
    weatherDetails: any;
    cityName: string;
    bgImage: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private storage: Storage,
        private networkProvider: NetworkProvider,
        private dataPassProvider: DataPassingProvider
    ) {
        // Load weather detail data and set it in view
        this.loadWeatherDetailsData();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad WeatherDetailsPage');
        // Load weather detail data and set it in view
        this.loadWeatherDetailsData();
    };

    // Load weather details data function
    loadWeatherDetailsData() {
        // Check network connection
        if (this.networkProvider.isOnline()) {
            this.weatherDetails = this.dataPassProvider.getData();
        } else {
            this.storage.get('WEATHER_DETAILS').then((value) => {
                console.log('Weather Details : ' + value);
                this.weatherDetails = JSON.parse(value);
            });
        }
        setTimeout(() => {
            if (this.weatherDetails) {
                this.bgImage = this.BgImage(this.weatherDetails.current.weather[0].main);
                this.cityName = this.weatherDetails.cityName;
            }
        }, 100);
    };

    windDirection = (deg) => {
        if (deg > 11.25 && deg < 33.75) {
            return "NNE";
        } else if (deg > 33.75 && deg < 56.25) {
            return "ENE";
        } else if (deg > 56.25 && deg < 78.75) {
            return "E";
        } else if (deg > 78.75 && deg < 101.25) {
            return "ESE";
        } else if (deg > 101.25 && deg < 123.75) {
            return "ESE";
        } else if (deg > 123.75 && deg < 146.25) {
            return "SE";
        } else if (deg > 146.25 && deg < 168.75) {
            return "SSE";
        } else if (deg > 168.75 && deg < 191.25) {
            return "S";
        } else if (deg > 191.25 && deg < 213.75) {
            return "SSW";
        } else if (deg > 213.75 && deg < 236.25) {
            return "SW";
        } else if (deg > 236.25 && deg < 258.75) {
            return "WSW";
        } else if (deg > 258.75 && deg < 281.25) {
            return "W";
        } else if (deg > 281.25 && deg < 303.75) {
            return "WNW";
        } else if (deg > 303.75 && deg < 326.25) {
            return "NW";
        } else if (deg > 326.25 && deg < 348.75) {
            return "NNW";
        } else {
            return "N";
        }
    }

    BgImage = (val) => {
        if (val == "Rain") {
            return './assets/imgs/rain.jpg';
        } else if (val == "Clear") {
            return './assets/imgs/clear.jpg';
        } else if (val == "Clouds") {
            return './assets/imgs/clouds.jpg';
        } else if (val == "Drizzle") {
            return './assets/imgs/drizzle.jpg';
        } else if (val == "Snow") {
            return './assets/imgs/snow.jpg';
        } else if (val == "ThunderStorm") {
            return './assets/imgs/thunder.jpg';
        } else {
            return './assets/imgs/clear.jpg';
        }
    }
}
