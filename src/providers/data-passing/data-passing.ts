
import { Injectable } from '@angular/core';

@Injectable()
export class DataPassingProvider {
    dataObject: any;

    constructor() {
        console.log('Hello DataPassingProvider Provider');
    }

    // Set data
    setData(object) {
        this.dataObject = object;
    };

    // Get data
    getData() {
        return this.dataObject;
    }
}
