import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';

@Injectable()
export class WeatherProvider {

    // API key for weather details
    apiKey = '809f9e8fd8a1f1c80efd5748c608ad2d';

    constructor(
        public http: HttpClient,
    ) {
        console.log('Hello WeatherProvider Provider');
    }

    // Get current weather details by calling weather api and return response
    currentWeather(lon: number, lat: number): Observable<any> {
        const currentInfo = this.http.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=${this.apiKey}`);
        return Observable.forkJoin([currentInfo])
            .map(responses => {
                return [].concat(...responses);
            });
    };
}
